// First example of a function.
function sayHello() {
    console.log("Hello!");
}

sayHello();

// function declaration 
function myFunc(a,b) {
    return (a+b);
}
myFunc(2,4);

// function expression
var myFunc = function(a,b) {
    return(a+b);
};
myFunc(2,4);