// First example of a function.
function sayHello() {
    console.log("Hello!");
}

sayHello();

// Another Syntax 

// function declaration 
function myFunc(a,b) {
    return (a+b);
}
myFunc(2,4);

// function expression
var myFunc = function(a,b) {
    return(a+b);
};
myFunc(2,4);

//Another Syntax V2

// function declaration
function capitalize(str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
}

var word = "hi";

capitalize(word);

// function expression
var capitalize = function (str) {
    return str.charAt(0).toUpperCase() + str.slice(1);
};

// Small Functions Quiz

// 1. What will be the result of:

function test(x,y) {
    return y-x;
}
test(10,40);

// Output = 30

// 2. What will be the result of:
function test (x) {
    return x*2;
    console.log(x);
    return x / 2;
}

test(40);

// Output = 80

// Functions Problem Set
isEven(4);
isEven(21);
isEven(68);
isEven(333);
// Solution
function isEven() {
    if(num % 2 === 0) {
        return true;
    }
    else {
        return false;
    }
}
//

//factorial is 4 is 4 x 3 x 2 x 1 = 24
factorial(5);
factorial(2);
factorial(10);
factorial(0);
// Solution

function factorial(num) {
    var result = 1;
    
    for(var i = 2; i <= num; i++) {
        result *= i;
    }
    return result;
}

//


// kebabToSnake()
// or, replace - with _
kebabToSnake("hello-world");
kebabToSnake("dogs-are-awesome");
kebabToSnake("blah");
// Solution
function kebabToSnake(str) {
    var newStr = str.replace(/-/g , "_");
    return newStr;
}
//